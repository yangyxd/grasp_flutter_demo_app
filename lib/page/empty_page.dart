import 'package:flutter/material.dart';
import 'package:grasp_flutter_app/utils/utils.dart';

class EmptyPage extends StatefulWidget {
  final String title;

  const EmptyPage({Key key, this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState()  => _EmptyPageState();
}

class _EmptyPageState extends State<EmptyPage> {

  @override
  Widget build(BuildContext context) {
    return Common.buildTopScaffold(
      context,
      title: Text(widget.title)
    );
  }

}