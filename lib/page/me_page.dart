import 'package:flutter/material.dart';
import 'package:grasp_flutter_app/utils/local/local_string_en.dart';
import 'package:grasp_flutter_app/utils/local/local_string_zh.dart';
import 'package:grasp_flutter_app/utils/utils.dart';

class MePage extends StatefulWidget {
  const MePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState()  => _PageState();
}

class _PageState extends State<MePage> {

  @override
  Widget build(BuildContext context) {
    return Common.buildTopScaffold(
      context,
      title: Text(Utils.getLocale(context).me),
      body: buildBody(),
    );
  }

  Widget buildBody() {
    return StoreBuilder<SysState>(builder: (context, store) {
      return ListView(
        padding: const EdgeInsets.only(top: 16.0),
        children: <Widget>[
          buildItem(Utils.getLocale(context).theme, Container(width: 24.0, alignment: Alignment.center, child: Container(width: 16.0, height: 16.0, color: Styles.primaryValue)), () {
            showThemeDialog(context, store);
          }),
          buildItem(Utils.getLocale(context).language, Icon(Icons.language), () {
            DialogEx.showList(context, Utils.getLocale(context).theme, [
              DialogItem(text: "Default", onTap: () {
                changeLocal(store, 0);
              }),
              DialogItem(text: "English", isSelected: Utils.getLocale(context) is LocalStringEn, onTap: () {
                changeLocal(store, 2);
              }),
              DialogItem(text: "简体中文", isSelected: Utils.getLocale(context) is LocalStringZH, onTap: () {
                changeLocal(store, 1);
              }),
            ]);
          })
        ],
      );
    });
  }

  Widget buildItem(String title, Widget leading, [VoidCallback onTap]) {
    return ListTile(
      title: Text(title),
      leading: leading,
      trailing: Icon(Icons.keyboard_arrow_right, color: Styles.lineColor),
      onTap: onTap,
    );
  }

  void changeLocal(store, int index) {
    Utils.changeLocale(store, index);
    LocalStorage.save(Config.LOCALE, index.toString());
  }

  void showThemeDialog(BuildContext context, Store store) {
    showDialog(context: context, builder: (context) {
      List<String> list = [
        Utils.getLocale(context).home_theme_default,
        Utils.getLocale(context).home_theme_1,
        Utils.getLocale(context).home_theme_2,
        Utils.getLocale(context).home_theme_3,
        Utils.getLocale(context).home_theme_4,
        Utils.getLocale(context).home_theme_5,
        Utils.getLocale(context).home_theme_6,
      ];
      final _colorList = Utils.getThemeListColor();
      return MediaQuery(
        data: MediaQueryData.fromWindow(WidgetsBinding.instance.window),
        child: AlertDialog(
          title: Text(Utils.getLocale(context).theme),
          content: Container(
            child: SingleChildScrollView(
              child: Column(
                children: buildItems(store, list, _colorList),
              ),
            ),
          )
        ),
      );
    });
  }

  List<Widget> buildItems(Store store, list, colorList) {
    List<Widget> items = [];
    for (int i=0; i<list.length; i++) {
      items.add(ListTile(
        leading: Container(width: 24.0, alignment: Alignment.center, child: Container(width: 16.0, height: 16.0, color: colorList[i])),
        title: Text(list[i]),
        onTap: () {
          Utils.pushTheme(store, i);
          LocalStorage.save(Config.THEME_COLOR, i.toString());
        },
      ));
    }
    return items;
  }

}