import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

import '../utils.dart';
import '../model/user.dart';
import 'locale_redux.dart';
import 'theme_redux.dart';

/// 全局Redux store 状态数据对象
class SysState {
  User user;

  /// 主题
  ThemeData themeData;

  /// 语言
  Locale locale;

  /// 当前手机平台默认语言
  Locale platformLocale;

  /// 构造方法
  SysState({this.themeData, this.locale, this.user});


  /// 创建 Reducer
  static SysState appReducer(SysState state, action) {
    return SysState(
      user: UserReducer(state.user, action),
      ///通过自定义 ThemeDataReducer 将 SysState 内的 themeData 和 action 关联在一起
      themeData: themeDataReducer(state.themeData, action),
      ///通过自定义 LocaleReducer 将 SysState 内的 locale 和 action 关联在一起
      locale: localeReducer(state.locale, action),
    );
  }

  static final List<Middleware<SysState>> middleware = [
    //EpicMiddleware<SysState>(UserInfoEpic()),
    //UserInfoMiddleware(),
  ];
}
