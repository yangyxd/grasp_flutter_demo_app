class Consts {

  static const String version = "1.0.0";
  static const String appName = "Grasp APP";
  static const String appTitle = "管家婆手机版";

  /// 调试标识
  static const bool debug = true;

  static const PAGE_SIZE = 20;

  static final double MILLIS_LIMIT = 1000.0;
  static final double SECONDS_LIMIT = 60 * MILLIS_LIMIT;
  static final double MINUTES_LIMIT = 60 * SECONDS_LIMIT;
  static final double HOURS_LIMIT = 24 * MINUTES_LIMIT;
  static final double DAYS_LIMIT = 30 * HOURS_LIMIT;


  ///网络错误
  static const NETWORK_ERROR = -1;
  ///网络超时
  static const NETWORK_TIMEOUT = -2;
  ///网络返回数据格式化一次
  static const NETWORK_JSON_EXCEPTION = -3;

  static const SUCCESS = 200;


  /// 是否需要弹提示
  static const NOT_TIP_KEY = "noTip";
}
