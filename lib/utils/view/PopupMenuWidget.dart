import 'package:flutter/material.dart';

class PopupMenuWidget extends StatefulWidget {
  const PopupMenuWidget({
    Key key, this.child, this.onPopup,
  });

  final Widget child;
  final ValueChanged<State> onPopup;

  @override
  State<StatefulWidget> createState() {
    return _PopupMenuWidgetState();
  }
}

class _PopupMenuWidgetState extends State<PopupMenuWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: widget.child,
      behavior: HitTestBehavior.opaque,
      onTap: widget.onPopup == null ? null : () {
        widget.onPopup(this);
      },
    );
  }

}