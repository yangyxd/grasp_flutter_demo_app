import 'package:flutter/material.dart';

import '../styles.dart';

class DialogEx {
  /// 生成一个等待动画
  static Widget buildCircularProgress({String text,
    Color color = const Color(0x1f000000),
    TextStyle style,
    Color bgcolor = const Color(0xbf303030)})
  {
    var v;
    if (text == null || text.isEmpty)
      v = new CircularProgressIndicator(strokeWidth: 2.0);
    else
      v = new Wrap(
        direction: Axis.vertical,
        crossAxisAlignment: WrapCrossAlignment.center,
        spacing: 8.0,
        children: <Widget>[
          new CircularProgressIndicator(strokeWidth: 2.0),
          new Text(text, style: style,)
        ],
      );

    return new Positioned.fill(child: new Container(
      color: color,
      child: new Center(
        child: new ConstrainedBox(
          constraints: const BoxConstraints(minWidth: 36.0, minHeight: 36.0),
          child: new Material(
            color: bgcolor,
            child: new Container(
              padding: const EdgeInsets.all(16.0),
              child: v,
            ),
            shape: const RoundedRectangleBorder(
                borderRadius: const BorderRadius.all(const Radius.circular(9.0))
            ),
          ),
        ),
      ),
    ));

  }

  static const double _DefaultIconSize = 32.0;
  static const double _DefaultIconSpace = 16.0;

  /// 列表显示对话框
  static show(BuildContext context, String title, List<Widget> items) {
    showDialog(
        context: context,
        builder: (BuildContext context)
    {
      return new SimpleDialog(title: title == null ? null : new Text(title), children: items);
    });
  }

  /// 列表显示对话框
  static showList(BuildContext context, String title, List<DialogItem> items, {Color iconColor, double iconSize = _DefaultIconSize,
    double spaceing = _DefaultIconSpace, TextStyle textStyle})
  {
    show(context, title, DialogDemoItem.buildItems(items,
      iconColor: iconColor,
      iconSize: iconSize,
      spaceing: spaceing,
      textStyle: textStyle,
    ));
  }

  static Map<Key, BuildContext> dialogMap = {};

  /// 显示等待对话框
  static Future<T> showWaitDialog<T>({
    Key key,
    BuildContext context,
    Widget msg,
    Color barrierColor = Colors.black54,
    Color dialogColor = Colors.black38,
    bool barrierDismissible = true
  }) async {
    if (key == null)
      key = Key(context.hashCode.toString());
    if (dialogMap.containsKey(key))
      return null;
    dialogMap[key] = context;
    var v = await showGeneralDialog<T>(
      context: context,
      pageBuilder: (BuildContext buildContext, Animation<double> animation, Animation<double> secondaryAnimation) {
        final ThemeData theme = Theme.of(context, shadowThemeOnly: true);
        final Widget pageChild =  Stack(
          alignment: Alignment.center,
          children: [
            Container(
              constraints: BoxConstraints(
                minWidth: 96.0,
                minHeight: 96.0,
              ),
              padding: const EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: dialogColor,
                  borderRadius: BorderRadius.circular(16.0)
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  CircularProgressIndicator(strokeWidth: 2.0),
                  SizedBox(
                    height: msg == null ? 0.0 : 8.0,
                  ),
                  DefaultTextStyle(
                    child: msg ?? SizedBox(width: 50.0),
                    style: TextStyle(color: Colors.white70),
                  )
                ],
              ),
            )
          ],
        );
        return SafeArea(
          child: Builder(
              builder: (BuildContext context) {
                return theme != null
                    ? Theme(data: theme, child: pageChild)
                    : pageChild;
              }
          ),
        );
      },
      barrierDismissible: barrierDismissible,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: barrierColor,
      transitionDuration: const Duration(milliseconds: 150),
      transitionBuilder: _buildMaterialDialogTransitions,
    );
    if (dialogMap.containsKey(key))
      dialogMap.remove(key);
    return v;
  }

  /// 关闭等待对话框
  static void dismissWaitDialog({Key key}) {
    if (key == null && dialogMap.isEmpty)
      return;
    if (key == null) {
      List keys = dialogMap.keys.toList();
      key = keys.last;
    }
    if (dialogMap.containsKey(key)) {
      BuildContext context = dialogMap[key];
      dialogMap.remove(key);
      if (context == null)
        return;
      Navigator.pop(context);
    }
  }

  static Widget _buildMaterialDialogTransitions(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return FadeTransition(
      opacity: CurvedAnimation(
        parent: animation,
        curve: Curves.easeOut,
      ),
      child: child,
    );
  }

}

/// 列表对话框列表项 (带图标)
class DialogDemoItem extends StatelessWidget {
  const DialogDemoItem({
    Key key, this.icon, this.color, this.text, this.onPressed,
    this.iconSize = DialogEx._DefaultIconSize,
    this.spaceing = DialogEx._DefaultIconSpace,
    this.textStyle,
    this.clickPop = true,
    this.isSelected = false,
  }) : super(key: key);

  final IconData icon;
  final Color color;
  final String text;
  final VoidCallback onPressed;
  final double iconSize, spaceing;
  final TextStyle textStyle;
  final bool clickPop;
  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    var view = (spaceing > 0.0) ? new Padding(
      padding: new EdgeInsets.only(left: spaceing),
      child: new Text(text, style: textStyle),
    ) : new Text(text, style: textStyle);
    return new SimpleDialogOption(
      onPressed: onPressed == null ? null : (clickPop ? () {
        Navigator.pop(context);
        onPressed();
      } : onPressed),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new Icon(icon, size: iconSize, color: isSelected ? Styles.primaryValue : color),
          (isSelected ? DefaultTextStyle(style: TextStyle(color: Styles.primaryValue), child: view) :view),
        ],
      ),
    );
  }

  static List<Widget> buildItems(List<DialogItem> items, {Color iconColor, double iconSize = DialogEx._DefaultIconSize, double spaceing = DialogEx._DefaultIconSpace, TextStyle textStyle}) {
    List<Widget> v = [];
    if (items != null && items.length > 0) {
      for (int i=0; i<items.length; i++) {
        if (items[i].isLine)
          v.add(new Divider());
        else
          v.add(new DialogDemoItem(
            icon: items[i].icon,
            text: items[i].text,
            isSelected: items[i].isSelected,
            onPressed: items[i].onTap,
            color: iconColor,
            iconSize: iconSize,
            textStyle: textStyle,
            spaceing: spaceing,
        ));
      }
    }
    return v;
  }
}

class DialogItem {
  final String text;
  final IconData icon;
  final VoidCallback onTap;
  final bool isLine;
  final bool isSelected;
  const DialogItem({this.text, this.icon, this.onTap, this.isLine = false, this.isSelected = false});
}

