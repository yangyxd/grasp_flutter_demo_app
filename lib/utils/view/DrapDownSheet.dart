import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

class DrapDownPopPosition {
  DrapDownPopPosition._();

  static const int top = 0;
  static const int bottom = 1;
}

Future<T> showModalDrapDownSheet<T>({
  @required BuildContext context,
  @required WidgetBuilder builder,
  bool clickEmptyPop,
  int position = DrapDownPopPosition.top,
  double offset = 0.0,
  double elevation = 8.0,
  double paddingOffset,
}) {
  assert(context != null);
  assert(builder != null);
  assert(debugCheckHasMaterialLocalizations(context));
  return Navigator.push(context, _ModalDrapDownSheetRoute<T>(
    builder: builder,
    clickEmptyPop: clickEmptyPop,
    position: position,
    offset: offset,
    paddingOffset: paddingOffset,
    elevation: elevation,
    theme: Theme.of(context, shadowThemeOnly: true),
    barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
  ));
}

const Duration _kDrapDownSheetDuration = Duration(milliseconds: 200);
const double _kMinFlingVelocity = 700.0;
const double _kCloseProgressThreshold = 0.5;

class _ModalDrapDownSheetRoute<T> extends PopupRoute<T> {
  _ModalDrapDownSheetRoute({
    this.builder,
    this.theme,
    this.position,
    this.offset,
    this.elevation,
    this.paddingOffset,
    this.barrierLabel,
    this.clickEmptyPop,
    RouteSettings settings,
  }) : super(settings: settings);

  final WidgetBuilder builder;
  final ThemeData theme;
  final bool clickEmptyPop;
  final int position;
  final double offset;
  final double elevation;
  final double paddingOffset;

  @override
  Duration get transitionDuration => _kDrapDownSheetDuration;

  @override
  bool get barrierDismissible => true;

  @override
  final String barrierLabel;

  @override
  Color get barrierColor => Color(0x00101010);

  AnimationController _animationController;

  @override
  AnimationController createAnimationController() {
    assert(_animationController == null);
    _animationController = DrapDownSheet.createAnimationController(navigator.overlay);
    return _animationController;
  }

  @override
  Widget buildPage(BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
    // By definition, the right sheet is aligned to the right of the page
    // and isn't exposed to the top padding of the MediaQuery.
    Widget DrapDownSheet = MediaQuery.removePadding(
      context: context,
      removeTop: false,
      child: Stack(
        children: <Widget>[
          IgnorePointer(child: AnimatedBuilder(
            animation: _animationController,
            builder: (BuildContext context, Widget child) {
              return Container(margin: EdgeInsets.only(top: offset + paddingOffset ?? 0.0), color: Colors.black38.withAlpha((animation.value * 80).round()));
            },
          )),
          Container(
            child: _ModalDrapDownSheet<T>(route: this, clickEmptyPop: this.clickEmptyPop ?? true),
            transform: Matrix4.translationValues(0.0, offset, 0.0),
          ),
        ],
      ),
    );
    if (theme != null)
      DrapDownSheet = Theme(data: theme, child: DrapDownSheet);
    return DrapDownSheet;
  }
}

class _ModalDrapDownSheet<T> extends StatefulWidget {
  const _ModalDrapDownSheet({ Key key, this.route, this.clickEmptyPop = true}) : super(key: key);

  final _ModalDrapDownSheetRoute<T> route;
  final bool clickEmptyPop;

  @override
  _ModalDrapDownSheetState<T> createState() => _ModalDrapDownSheetState<T>();
}

class _ModalDrapDownSheetState<T> extends State<_ModalDrapDownSheet<T>> {
  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQuery = MediaQuery.of(context);
    final MaterialLocalizations localizations = MaterialLocalizations.of(context);
    String routeLabel;
    switch (defaultTargetPlatform) {
      case TargetPlatform.iOS:
        routeLabel = '';
        break;
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        routeLabel = localizations.dialogLabel;
        break;
    }

    return GestureDetector(
        excludeFromSemantics: true,
        onTap: widget.clickEmptyPop ? () => Navigator.pop(context) : null,
        child: AnimatedBuilder(
            animation: widget.route.animation,
            builder: (BuildContext context, Widget child) {
              // Disable the initial animation when accessible navigation is on so
              // that the semantics are added to the tree at the correct time.
              final double animationValue = mediaQuery.accessibleNavigation ? 1.0 : widget.route.animation.value;
              return Semantics(
                scopesRoute: true,
                namesRoute: true,
                label: routeLabel,
                explicitChildNodes: true,
                child: ClipRect(
                  child: CustomSingleChildLayout(
                    delegate: _ModalDrapDownSheetLayout(animationValue, widget.route.position),
                    child: DrapDownSheet(
                      animationController: widget.route._animationController,
                      onClosing: () => Navigator.pop(context),
                      builder: widget.route.builder,
                      elevation: widget.route.elevation ?? 8.0,
                    ),
                  ),
                ),
              );
            }
        )
    );
  }
}

class _ModalDrapDownSheetLayout extends SingleChildLayoutDelegate {
  _ModalDrapDownSheetLayout(this.progress, this.position);

  final double progress;
  final int position;

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    return BoxConstraints(
        minWidth: constraints.maxWidth,
        maxWidth: constraints.maxWidth,
        minHeight: 0.0,
        maxHeight: constraints.maxHeight
    );
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    if (position == DrapDownPopPosition.bottom)
      return Offset(0.0, size.height - childSize.height * progress);
    if (position == DrapDownPopPosition.top)
      return Offset(0.0, -childSize.height * (1.0 - progress));
    return Offset(0.0, 0.0);
  }

  @override
  bool shouldRelayout(_ModalDrapDownSheetLayout oldDelegate) {
    return progress != oldDelegate.progress;
  }
}

class DrapDownSheet extends StatefulWidget {
  /// Creates a right sheet.
  ///
  /// Typically, right sheets are created implicitly by
  /// [ScaffoldState.showDrapDownSheet], for persistent right sheets, or by
  /// [showModalDrapDownSheet], for modal right sheets.
  const DrapDownSheet({
    Key key,
    this.animationController,
    this.enableDrag = true,
    this.elevation = 8.0,
    @required this.onClosing,
    @required this.builder
  }) : assert(enableDrag != null),
        assert(onClosing != null),
        assert(builder != null),
        assert(elevation != null),
        super(key: key);

  /// The animation that controls the right sheet's position.
  ///
  /// The DrapDownSheet widget will manipulate the position of this animation, it
  /// is not just a passive observer.
  final AnimationController animationController;

  /// Called when the right sheet begins to close.
  ///
  /// A right sheet might be prevented from closing (e.g., by user
  /// interaction) even after this callback is called. For this reason, this
  /// callback might be call multiple times for a given right sheet.
  final VoidCallback onClosing;

  /// A builder for the contents of the sheet.
  ///
  /// The right sheet will wrap the widget produced by this builder in a
  /// [Material] widget.
  final WidgetBuilder builder;

  /// If true, the right sheet can dragged up and down and dismissed by swiping
  /// downards.
  ///
  /// Default is true.
  final bool enableDrag;

  /// The z-coordinate at which to place this material. This controls the size
  /// of the shadow below the material.
  ///
  /// Defaults to 0.
  final double elevation;

  @override
  _DrapDownSheetState createState() => _DrapDownSheetState();

  /// Creates an animation controller suitable for controlling a [DrapDownSheet].
  static AnimationController createAnimationController(TickerProvider vsync) {
    return AnimationController(
      duration: _kDrapDownSheetDuration,
      debugLabel: 'DrapDownSheet',
      vsync: vsync,
    );
  }
}

class _DrapDownSheetState extends State<DrapDownSheet> {

  final GlobalKey _childKey = GlobalKey(debugLabel: 'DrapDownSheet child');

  double get _childWidth {
    final RenderBox renderBox = _childKey.currentContext.findRenderObject();
    return renderBox.size.width;
  }

  bool get _dismissUnderway => widget.animationController.status == AnimationStatus.reverse;

  void _handleDragUpdate(DragUpdateDetails details) {
    if (_dismissUnderway)
      return;
    widget.animationController.value -= details.primaryDelta / (_childWidth ?? details.primaryDelta);
  }

  void _handleDragEnd(DragEndDetails details) {
    if (_dismissUnderway)
      return;
    if (details.velocity.pixelsPerSecond.dy > _kMinFlingVelocity) {
      final double flingVelocity = -details.velocity.pixelsPerSecond.dx / _childWidth;
      if (widget.animationController.value > 0.0)
        widget.animationController.fling(velocity: flingVelocity);
      if (flingVelocity < 0.0)
        widget.onClosing();
    } else if (widget.animationController.value < _kCloseProgressThreshold) {
      if (widget.animationController.value > 0.0)
        widget.animationController.fling(velocity: -1.0);
      widget.onClosing();
    } else {
      widget.animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    final Widget sheet = Material(
      key: _childKey,
      color: Colors.transparent,
      elevation: widget.elevation,
      child: widget.builder(context),
    );
    return !widget.enableDrag ? sheet : GestureDetector(
      onVerticalDragUpdate: _handleDragUpdate,
      onVerticalDragEnd: _handleDragEnd,
      child: sheet,
      excludeFromSemantics: true,
    );
  }
}