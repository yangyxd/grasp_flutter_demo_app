import 'package:flutter/material.dart';

import '../utils.dart';

/// Icon Text
class IconText extends StatelessWidget {
  final String text;
  final Icon icon;
  final double spacing;
  final TextStyle style;
  final Color bgcolor;
  final EdgeInsets padding;
  final TextOverflow overflow;
  final Axis direction;
  final VerticalDirection verticalDirection;
  final String tooltip;
  final bool useWrap;
  final bool iconLast;

  IconText({this.text,
    this.icon,
    this.style,
    this.spacing = 4.0,
    this.bgcolor,
    this.padding = const EdgeInsets.only(right: 6.0),
    this.overflow = TextOverflow.clip,
    this.verticalDirection = VerticalDirection.down,
    this.tooltip,
    this.direction = Axis.horizontal,
    this.useWrap = false,
    this.iconLast = false,
  }) : assert(icon != null || text != null);

  @override
  Widget build(BuildContext context) {
    List<Widget> views = [];
    if (icon != null && !iconLast)
      views.add(icon);

    if (text != null && text.length > 0) {
      if (useWrap)
        views.add(new Text(text, style: style,
            overflow: this.overflow));
      else {
        if (icon != null && !iconLast)
          views.add(new SizedBox(width: this.spacing,));
        if (this.overflow == TextOverflow.ellipsis)
          views.add(new Expanded(child: new Container(
            child: Text(text, style: Styles.smallText,
                overflow: this.overflow),
          )));
        else
          views.add(new Text(text, style:Styles.smallText,
              overflow: this.overflow));
        if (icon != null && iconLast)
          views.add(new SizedBox(width: this.spacing,));
      }
    }

    if (icon != null && iconLast)
      views.add(icon);

    Widget result;
    if (useWrap) {
      result = new Wrap(
        children: views,
        crossAxisAlignment: WrapCrossAlignment.center,
        spacing: this.spacing,
        direction: this.direction,
        verticalDirection: this.verticalDirection,
      );
    } else if (this.direction == Axis.horizontal) {
      result = new Row(
        children: views,
        crossAxisAlignment: CrossAxisAlignment.center,
        verticalDirection: this.verticalDirection,
      );
    } else {
      result = new Column(
        children: views,
        crossAxisAlignment: CrossAxisAlignment.center,
        verticalDirection: this.verticalDirection,
      );
    }

    if (tooltip != null) {
      result = new Tooltip(
          message: tooltip,
          child: result
      );
    }

    if (bgcolor == null && padding == null) {
      return result;
    } else {
      return new Container(
        child: result,
        color: bgcolor,
        padding: this.padding,
      );
    }
  }
}
