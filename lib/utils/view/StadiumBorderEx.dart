// import 'dart:ui' as ui show lerpDouble;
import 'package:flutter/material.dart';

class StadiumBorderEx extends ShapeBorder {
  /// Create a stadium border.
  ///
  /// The [side] argument must not be null.
  const StadiumBorderEx({this.side = BorderSide.none, this.borderRadius}) : assert(side != null);

  /// The style of this border.
  final BorderSide side;
  final BorderRadius borderRadius;

  @override
  EdgeInsetsGeometry get dimensions {
    return new EdgeInsets.all(side.width);
  }

  @override
  ShapeBorder scale(double t) => new StadiumBorderEx(side: side.scale(t));

  @override
  ShapeBorder lerpFrom(ShapeBorder a, double t) {
    assert(t != null);
    if (a is StadiumBorderEx)
      return new StadiumBorderEx(side: BorderSide.lerp(a.side, side, t));
    return super.lerpFrom(a, t);
  }

  @override
  ShapeBorder lerpTo(ShapeBorder b, double t) {
    assert(t != null);
    if (b is StadiumBorderEx)
      return new StadiumBorderEx(side: BorderSide.lerp(side, b.side, t));
    return super.lerpTo(b, t);
  }

  @override
  Path getInnerPath(Rect rect, { TextDirection textDirection }) {
    if (borderRadius == null) {
      return new Path()
        ..addRRect(new RRect.fromRectAndRadius(rect, Radius.circular(rect.shortestSide / 2.0)));
    }
    return new Path()
      ..addRRect(borderRadius.toRRect(rect));
  }

  @override
  Path getOuterPath(Rect rect, { TextDirection textDirection }) {
    if (borderRadius == null) {
      return new Path()
        ..addRRect(new RRect.fromRectAndRadius(rect, Radius.circular(rect.shortestSide / 2.0)));
    }
    return new Path()
      ..addRRect(borderRadius.toRRect(rect));
  }

  @override
  void paint(Canvas canvas, Rect rect, { TextDirection textDirection }) {
    switch (side.style) {
      case BorderStyle.none:
        break;
      case BorderStyle.solid:
        final RRect r = borderRadius == null ? RRect.fromRectAndRadius(rect, Radius.circular(rect.shortestSide / 2.0)).deflate(side.width / 2.0) :
          borderRadius.toRRect(rect);
        canvas.drawRRect(r, side.toPaint());
    }
  }

  @override
  bool operator ==(dynamic other) {
    if (runtimeType != other.runtimeType)
      return false;
    final StadiumBorderEx typedOther = other;
    return side == typedOther.side;
  }

  @override
  int get hashCode => side.hashCode;

  @override
  String toString() {
    return '$runtimeType($side)';
  }
}

