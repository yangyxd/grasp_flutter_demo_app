import 'package:flutter/material.dart';
import '../styles.dart';

class SearchPopupList extends StatefulWidget {

  const SearchPopupList({
    Key key,
    this.initialFilter,
    this.itemCount,
    this.itemExtent,
    @required this.itemBuilder,
    this.onFilter,
  }) : super (key: key);

  final String initialFilter;
  final ValueChanged<String> onFilter;
  final IndexedWidgetBuilder itemBuilder;
  final int itemCount;
  final double itemExtent;

  @override
  State<StatefulWidget> createState() {
    return _SearchPopupListState();
  }
}

class _SearchPopupListState extends State<SearchPopupList> {
  TextEditingController editController;

  @override
  void initState() {
    super.initState();
    editController = new TextEditingController(text: widget.initialFilter);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var edit = TextField(
      style: Styles.middleText,
      controller: editController,
      decoration: new InputDecoration(
          border: InputBorder.none,
          hintText: "搜索内容",
          hintStyle: Styles.middleSubLightText,
          contentPadding: EdgeInsets.only(left: 0.0, right: 0.0, top: 6.0, bottom: 0.0),
          suffixIcon: editController.text == null || editController.text.isEmpty
              ? null
              : IconButton(
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 0.0, bottom: 0.0),
              icon: Icon(Icons.close, color: Styles.subTextColor),
              highlightColor: Styles.nullColor,
              onPressed: () {
                editController.clear();
                doFilter(null);
              }),
          prefixIcon: new Container(
            //padding: EdgeInsets.only(left: 16.0, right: 8.0, top: 0.0, bottom: 0.0),
              child: new Icon(Icons.search, color: Styles.subTextColor)
          )
      ),
      onChanged: (text) {
        doFilter(editController.text);
      },
    );

    return Column(
      children: <Widget>[
        Container(
          height: 36.0,
          child: edit,
          margin: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
            color: Styles.mainBackgroundColor,
            borderRadius: BorderRadius.circular(15.0),
            border: Border.all(color: Colors.black12, width: 0.5)
          ),
        ),
        Expanded(
          child: ListView.builder(
            padding: const EdgeInsets.all(0.0),
            itemBuilder: widget.itemBuilder,
            itemCount: widget.itemCount,
            itemExtent: widget.itemExtent,
          ),
        ),
        Container(
          height: 48.0,
          width: double.infinity,
          child: FlatButton(
              onPressed: () {
                Navigator.pop(context);
              }, child: Text("取消", style: Styles.normalTextLight)
          ),
        ),
      ],
    );
  }

  void doFilter(String key) {
    setState(() {
      if (widget.onFilter == null) return;
      widget.onFilter(key);
    });
  }
}

