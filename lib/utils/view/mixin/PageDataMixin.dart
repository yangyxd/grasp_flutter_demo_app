import 'package:flutter/material.dart';

abstract class PageDataMixin<T extends Object> {
  factory PageDataMixin._() => null;

  /// 数据
  List<T> get data => getData();

  @protected
  @mustCallSuper
  List<T> getData();

  /// 获取标题
  int getDataLength() {
    return data == null ? 0 : data.length;
  }

  bool getDataIsNull() {
    return data == null;
  }

  // 生成消息列表项
  Widget buildListItemViewIndex(BuildContext context, int index) {
    return buildListItemView(context, data[index], index);
  }

  // 生成消息列表项
  @protected
  @mustCallSuper
  Widget buildListItemView(BuildContext context, T item, int index);
}

