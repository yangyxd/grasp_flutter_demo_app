import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';

import '../utils.dart';
import 'StadiumBorderEx.dart';

typedef CheckedCallback = bool Function(bool newValue);

class GButton extends StatelessWidget {
  /// 按钮组件，支持冷却时间设定
  GButton({Key key,
    this.child,
    this.onPressed,
    this.width,
    this.height,
    this.styles,
    this.color,
    this.disabledColor,
    this.border,
    this.textColor,
  }) : super (key: key);

  final Widget child;
  final VoidCallback onPressed;
  final double width, height;
  final TextStyle styles;
  final Color color;
  final Color disabledColor;
  final Color textColor;
  final ShapeBorder border;

  @override
  Widget build(BuildContext context) {
    final ButtonThemeData buttonTheme = ButtonTheme.of(context);
    return RawMaterialButton(
      constraints: height != null ? BoxConstraints(
        minWidth: buttonTheme.minWidth,
        minHeight: height,
      ) : BoxConstraints(minWidth: buttonTheme.minWidth, minHeight: buttonTheme.height),
      onPressed: onPressed,
      autofocus: false,

      elevation: 0.0,
      disabledElevation: 0.0,
      highlightElevation: 0.0,
      focusElevation: 0.0,
      hoverElevation: 0.0,

      shape: border ?? RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(3.0),
          side: BorderSide(color: onPressed == null ? Styles.lineColor : color ==  null ? Styles.subLightTextColor : Styles.colorLight(color, -0.1), width: 0.5)
      ),
      fillColor: onPressed == null ? ( disabledColor ?? Styles.lineColor ) : color ?? Styles.white,
      child: DefaultTextStyle(
        style: styles ?? TextStyle(fontSize: Styles.textWhiteMiddleSize, color: onPressed == null ? Styles.subTextColor : textColor ?? Styles.mainTextColor),
        child: child,
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(ColorProperty('textColor', textColor, defaultValue: null));
    properties.add(ColorProperty('color', color, defaultValue: null));
    properties.add(ColorProperty('disabledColor', Styles.lineColor, defaultValue: Styles.lineColor));
  }
}