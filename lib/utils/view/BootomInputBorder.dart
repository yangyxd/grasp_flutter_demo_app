import 'package:flutter/material.dart';

/// 文本框底部边线
class BootomInputBorder extends InputBorder {
  final Color color;
  final double width;

  BootomInputBorder(this.color, {this.width = 1.0}): super(borderSide: new BorderSide(color: color, width: width));

  @override
  BootomInputBorder copyWith(
      {BorderSide borderSide, BorderRadius borderRadius}) {
    return new BootomInputBorder(this.color, width: this.width);
  }

  @override
  bool get isOutline => false;

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.zero;

  @override
  BootomInputBorder scale(double t) =>
      BootomInputBorder(this.color, width: this.width);

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) {
    return new Path()..addRect(rect);
  }

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    return new Path()..addRect(rect);
  }

  @override
  void paint(
      Canvas canvas,
      Rect rect, {
        double gapStart,
        double gapExtent: 0.0,
        double gapPercentage: 0.0,
        TextDirection textDirection,
      }) {
    canvas.drawLine(rect.bottomLeft, rect.bottomRight, borderSide.toPaint());
  }
}