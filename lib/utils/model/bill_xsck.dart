import 'data_base.dart';

/// 单据 - 销售出库单据数据
class BillXSCKItem extends DataBase {
  BillXSCKItem(Map data) : super(data);

  /// 单号
  String get id => getItem("id");
  set id(String v) => setItem("id", v);

  /// 状态
  String get state => getItem("state");
  set state(String v) => setItem("state", v);

  /// 供应商
  String get supplier => getItem("supplier");
  set supplier(String v) => setItem("supplier", v);

  /// 出货仓库
  String get store => getItem("store");
  set store(String v) => setItem("store", v);

  /// 摘要
  String get remark => getItem("remark");
  set remark(String v) => setItem("remark", v);

  /// 金额
  double get money => getFloat("money");
  set money(double v) => setItem("money", v);

  DateTime get date => getDateTime("date");

  bool select = false;
}