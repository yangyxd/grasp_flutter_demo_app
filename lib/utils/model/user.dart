import 'data_base.dart';

/// 用户数据
class User extends DataBase {
  User(Map data) : super(data);

  String get name => getItem("name");
    set name(String v) => setItem("name", v);

  String get token => getItem("token");
    set token(String v) => setItem("token", v);

  static User empty() {
    return User({});
  }
}