import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math';

/// 样式 by YangYxd
class Styles {

  /// 默认 Elevation
  static const double elevation = 0.3;
  /// 边框粗细
  static const double borderSize = 0.3;
  /// 列表分隔线高度
  static const double lineSize = 0.35;

  static SystemUiOverlayStyle uiStyle = const SystemUiOverlayStyle(
    systemNavigationBarColor: const Color(0xff000000),
    systemNavigationBarDividerColor: null,
    statusBarColor: const Color(0x00000000),
    systemNavigationBarIconBrightness: Brightness.light,
    statusBarIconBrightness: Brightness.dark,
    statusBarBrightness: Brightness.dark,
  );

  /// 当前主题填充颜色
  static Color primaryValue = primaryThemeColor;
  static Color primaryContrastValue = textColorWhite;

  /// 默认主题颜色
  static const primaryThemeColor = const Color(0xfffb696a); // const Color(0xFF24292E);
  static const primaryLightValue = const Color(0xFF42464b);
  static const primaryDarkValue = const Color(0xFF121917);

  static const cardWhite = const Color(0xFFFFFFFF);
  static const textWhite = const Color(0xFFFFFFFF);
  static const miWhite = const Color(0xffececec);
  static const white = const Color(0xFFFFFFFF);
  static const actionBlue = const Color(0xff267aff);
  static const subTextColor = const Color(0xff959595);
  static const subLightTextColor = const Color(0xffc4c4c4);

  static const mainBackgroundColor = miWhite;

  static const mainTextColor = primaryDarkValue;
  static const textColorWhite = white;

  static const nullColor = const Color(0x00000000);
  static const lineColor = const Color(0xFFdddddd);

  static MaterialColor primarySwatch = MaterialColor(
    primaryValue.value,
    const <int, Color>{
      50: primaryLightValue,
      100: primaryLightValue,
      200: primaryLightValue,
      300: primaryLightValue,
      400: primaryLightValue,
      500: primaryThemeColor,
      600: primaryDarkValue,
      700: primaryDarkValue,
      800: primaryDarkValue,
      900: primaryDarkValue,
    },
  );

  static final List<Color> defaultThemeListColor = [
    primarySwatch,
    Colors.brown,
    Colors.blue,
    Colors.teal,
    Colors.amber,
    Colors.blueGrey,
    Colors.deepOrange,
  ];

  /// 超大字号
  static const textLagerSize = 30.0;
  /// 大字号
  static const textBigSize = 23.0;
  /// 标准文本大小（正文标题）
  static const textNormalSize = 18.0;
  /// 中间白色文本大小（正文内容）
  static const textWhiteMiddleSize = 16.0;
  /// 小字体（正文列表）
  static const textSmallSize = 14.0;
  /// 最小字体（正文描述）
  static const textMinSize = 12.0;


  /// 颜色亮度调节, offset 取值为 -1 ~ 1 之间
  static Color colorLight(Color value, double offset) {
    int v = (offset * 255).round();
    if (v > 0) {
      return Color.fromARGB(value.alpha, min(255, value.red + v), min(255, value.green + v), min(255, value.blue + v));
    } else {
      return Color.fromARGB(value.alpha, max(0, value.red + v), max(0, value.green + v), max(0, value.blue + v));
    }
  }

  static const minText = TextStyle(
    color: mainTextColor,
    fontSize: textMinSize,
  );

  static const minSubLightText = TextStyle(
    color: subLightTextColor,
    fontSize: textMinSize,
  );

  static const minSubText = TextStyle(
    color: subTextColor,
    fontSize: textMinSize,
  );

  static const minMiLightText = TextStyle(
    color: miWhite,
    fontSize: textMinSize,
  );

  static get minPrimaryText { return TextStyle(
    color: primaryValue,
    fontSize: textMinSize,
  ); }

  static get minPrimaryContrastText { return TextStyle(
    color: primaryContrastValue,
    fontSize: textMinSize,
  );}

  static const smallTextWhite = TextStyle(
    color: textColorWhite,
    fontSize: textSmallSize,
  );

  static const smallText = TextStyle(
    color: mainTextColor,
    fontSize: textSmallSize,
  );

  static const smallTextBold = TextStyle(
    color: mainTextColor,
    fontSize: textSmallSize,
    fontWeight: FontWeight.bold,
  );

  static const smallSubLightText = TextStyle(
    color: subLightTextColor,
    fontSize: textSmallSize,
  );

  static const smallActionLightText = TextStyle(
    color: actionBlue,
    fontSize: textSmallSize,
  );

  static const smallMiLightText = TextStyle(
    color: miWhite,
    fontSize: textSmallSize,
  );

  static TextStyle get smallPrimaryText { return TextStyle(
    color: primaryValue,
    fontSize: textSmallSize,
  );}

  static TextStyle get smallPrimaryContrastText { return TextStyle(
    color: primaryContrastValue,
    fontSize: textSmallSize,
  );}

  static const smallSubText = TextStyle(
    color: subTextColor,
    fontSize: textSmallSize,
  );

  static const middleText = TextStyle(
    color: mainTextColor,
    fontSize: textWhiteMiddleSize,
  );

  static const middleTextWhite = TextStyle(
    color: textColorWhite,
    fontSize: textWhiteMiddleSize,
  );

  static const middleSubText = TextStyle(
    color: subTextColor,
    fontSize: textWhiteMiddleSize,
  );

  static const middleSubLightText = TextStyle(
    color: subLightTextColor,
    fontSize: textWhiteMiddleSize,
  );

  static const middleTextBold = TextStyle(
    color: mainTextColor,
    fontSize: textWhiteMiddleSize,
    fontWeight: FontWeight.bold,
  );

  static const middleTextWhiteBold = TextStyle(
    color: textColorWhite,
    fontSize: textWhiteMiddleSize,
    fontWeight: FontWeight.bold,
  );

  static const middleSubTextBold = TextStyle(
    color: subTextColor,
    fontSize: textWhiteMiddleSize,
    fontWeight: FontWeight.bold,
  );

  static TextStyle get middleTextPrimary {return TextStyle(
    color: primaryThemeColor,
    fontSize: textWhiteMiddleSize,
  );}

  static TextStyle get middlePrimaryContrastText { return TextStyle(
    color: primaryContrastValue,
    fontSize: textWhiteMiddleSize,
  );}

  static const middleTextprimaryLight = TextStyle(
    color: primaryLightValue,
    fontSize: textWhiteMiddleSize,
  );

  static const normalText = TextStyle(
    color: mainTextColor,
    fontSize: textNormalSize,
  );

  static const normalTextBold = TextStyle(
    color: mainTextColor,
    fontSize: textNormalSize,
    fontWeight: FontWeight.bold,
  );

  static const normalSubText = TextStyle(
    color: subTextColor,
    fontSize: textNormalSize,
  );

  static const normalTextWhite = TextStyle(
    color: textColorWhite,
    fontSize: textNormalSize,
  );

  static TextStyle get normalPrimaryContrastText { return TextStyle(
    color: primaryContrastValue,
    fontSize: textNormalSize,
  );}

  static const normalTextMitWhiteBold = TextStyle(
    color: miWhite,
    fontSize: textNormalSize,
    fontWeight: FontWeight.bold,
  );

  static const normalTextActionWhiteBold = TextStyle(
    color: actionBlue,
    fontSize: textNormalSize,
    fontWeight: FontWeight.bold,
  );

  static const normalTextLight = TextStyle(
    color: primaryLightValue,
    fontSize: textNormalSize,
  );

  static const largeText = TextStyle(
    color: mainTextColor,
    fontSize: textBigSize,
  );

  static const largeTextBold = TextStyle(
    color: mainTextColor,
    fontSize: textBigSize,
    fontWeight: FontWeight.bold,
  );

  static const largeTextWhite = TextStyle(
    color: textColorWhite,
    fontSize: textBigSize,
  );

  static const largeTextWhiteBold = TextStyle(
    color: textColorWhite,
    fontSize: textBigSize,
    fontWeight: FontWeight.bold,
  );

  static const largeLargeTextWhite = TextStyle(
    color: textColorWhite,
    fontSize: textLagerSize,
    fontWeight: FontWeight.bold,
  );

  static TextStyle get largeLargeText { return TextStyle(
    color: primaryValue,
    fontSize: textLagerSize,
    fontWeight: FontWeight.bold,
  );}
  
}
