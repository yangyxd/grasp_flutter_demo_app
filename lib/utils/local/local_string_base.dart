abstract class LocalStringBase {
  String name;
  String ok = "ok";
  String cancel = "cancel";

  String share = "share";
  String print = "print";
  String me = "me";

  String home = "home";
  String commodity = "commodity";
  String bill = "bill";

  String page = "Page";
  String theme = "Theme";
  String language = "Language";

  String home_theme_default = "Default";
  String home_theme_1 = "Theme1";
  String home_theme_2 = "Theme2";
  String home_theme_3 = "Theme3";
  String home_theme_4 = "Theme4";
  String home_theme_5 = "Theme5";
  String home_theme_6 = "Theme6";

  String copySuccess = "Copy Success";

  String networkError_401 = "Http 401";
  String networkError_403 = "Http 403";
  String networkError_404 = "Http 404";
  String networkErrorTimeout = "Http timeout";
  String networkErrorUnknown = "Http unknown error";
  String networkError = "network error";

  String loadMoreNot = "nothing";
  String loadMoreText = "loading";

  String newBill_XSCKD = "New Bill XSCKD";

  String list_empty = "No data, click refresh";
  String listLoadFinish = "Total: %d, all loaded";
  String listLoadingTip = "Loaded: %d/%d, Loading...";
  String listLoadingTip2 = "Loaded: %d, Loading...";
}