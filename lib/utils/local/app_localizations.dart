import 'dart:ui';
import 'package:flutter/material.dart';
import 'local_string_base.dart';
import 'local_string_en.dart';
import 'local_string_zh.dart';

/// 自定义多语言实现
class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  /// 根据不同 locale.languageCode 加载不同语言对应
  static Map<String, LocalStringBase> _localizedValues = {
    'en': new LocalStringEn(),
    'zh': new LocalStringZH(),
  };

  LocalStringBase get currentLocalized {
    if(_localizedValues.containsKey(locale.languageCode)) {
      return _localizedValues[locale.languageCode];
    }
    return _localizedValues["en"];
  }

  /// 通过 Localizations 加载当前的 AppLocalizations
  /// 获取对应的 LocalStringBase
  static AppLocalizations of(BuildContext context) {
    return Localizations.of(context, AppLocalizations);
  }
}
