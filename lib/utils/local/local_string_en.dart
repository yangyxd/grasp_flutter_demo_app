import 'package:grasp_flutter_app/utils/local/local_string_base.dart';
import '../Consts.dart';

class LocalStringEn extends LocalStringBase {
  @override
  String name = "Grasp App";
  @override
  String ok = "ok";
  @override
  String cancel = "cancel";
  @override
  String share = "share";
  @override
  String print = "print";
  @override
  String home = "home";
}