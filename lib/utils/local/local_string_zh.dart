import 'package:flutter/cupertino.dart';

import 'local_string_base.dart';

class LocalStringZH extends LocalStringBase {
  String name = "管家婆手机版";
  String ok = "确定";
  String cancel = "取消";
  String share = "分享";
  String print = "打印";
  String home = "首页";
  String me = "我的";
  String commodity = "商品";
  String bill = "单据";

  String page = "页面";
  String theme = "主题";
  String language = "语言";

  String home_theme_default = "默认主题";
  String home_theme_1 = "主题1";
  String home_theme_2 = "主题2";
  String home_theme_3 = "主题3";
  String home_theme_4 = "主题4";
  String home_theme_5 = "主题5";
  String home_theme_6 = "主题6";

  String copySuccess = "已经复制到粘贴板";

  String networkError_401 = "[401错误可能: 未授权 \\ 授权登录失败 \\ 登录过期]";
  String networkError_403 = "403权限错误";
  String networkError_404 = "404错误";
  String networkErrorTimeout = "请求超时";
  String networkErrorUnknown = "其他异常";
  String networkError = "网络错误";

  String loadMoreNot = "没有更多数据";
  String loadMoreText = "正在加载更多";

  String newBill_XSCKD = "开销售出库单";

  String list_empty = "没有数据, 点击刷新";
  String listLoadFinish = "共%d项,已经全部加载";
  String listLoadingTip = "已加载%d/%d项, 正在加载...";
  String listLoadingTip2 = "已加载%d项, 正在加载...";
}