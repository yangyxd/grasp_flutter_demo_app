import 'package:dio/dio.dart';
import 'package:connectivity/connectivity.dart';
import '../utils.dart';


/// 错误拦截
class ErrorInterceptors extends InterceptorsWrapper {
  final Dio _dio;

  ErrorInterceptors(this._dio);

  @override
  onRequest(RequestOptions options) async {
    //没有网络
    var connectivityResult = await (new Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      return _dio.resolve(new ResultData(Utils.errorHandleFunction(Consts.NETWORK_ERROR, "", false), false, Consts.NETWORK_ERROR));
    }
    return options;
  }
}
