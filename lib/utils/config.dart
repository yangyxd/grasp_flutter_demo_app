class Config {
  static const TOKEN_KEY = "token";
  static const USER_NAME_KEY = "user-name";
  static const PW_KEY = "user-pw";
  static const LOCALE = "locale";
  static const USER_INFO = "user-info";
  static const THEME_COLOR = "theme-color";
  static const USER_BASIC_CODE = "user-basic-code";
}