import 'dart:async';

import '../model/bill_xsck.dart';
import '../utils.dart';
import 'dart:math';

///销售出库单数据存取对象
class BillXSCKDao {
  static List<BillXSCKItem> _datas;

  static void clear() {
    _datas = null;
  }

  /// 获取单据列表
  static getList(int page, void callback(List<BillXSCKItem> data)) async {
    await Future.delayed(Duration(milliseconds: Random().nextInt(3000) + 500), () {
      callback(makeList(page));
    });
  }

  static List<BillXSCKItem> makeList(int page) {
    if (_datas == null) {
      _datas = [];
      int dayOffset = 0;
      for (int i=0; i <Random().nextInt(100) + 30; i++) {
        DateTime date = Utils.dateIncDay(DateTime.now(), -dayOffset);
        BillXSCKItem item = BillXSCKItem({"date": "${Utils.dateToStr(date, true)}"});
        item.id = "XSD1908300000${sprintf("%03d", [i])}";
        item.state = "待审核";
        item.store = "默认仓库";
        item.money = 8.8;
        _datas.add(item);

        if (Random().nextInt(999) % 3 == 0)
          dayOffset++;
      }

      if (Consts.debug)
        print("_datas.length ${_datas.length}; page: $page");
    }

    int index = Consts.PAGE_SIZE * page;
    int count = min(Consts.PAGE_SIZE, _datas.length - index);
    if (count <= 0) return null;
    return _datas.sublist(index, index + count);
  }
}
