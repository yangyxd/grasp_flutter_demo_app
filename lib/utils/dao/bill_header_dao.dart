import 'package:grasp_flutter_app/utils/local/local_storage.dart';
import 'package:grasp_flutter_app/utils/model/data_base.dart';

import '../utils.dart';

class BillHeaderDao {
  const BillHeaderDao._();

  static const String _key = "bill_header_visible";

  static List<Map> headers = [
    {"name": "单据编号", "id": 0, "visible": 1, "hint": ""},
    {"name": "客户", "id": 1, "visible": 1, "hint": "点击选择客户"},
    {"name": "此前欠款", "id": 2, "visible": 1, "hint": "0"},
    {"name": "业务日期", "id": 3, "visible": 1, "hint": ""},
    {"name": "经手人", "id": 4, "visible": 1, "hint": ""},
    {"name": "出货仓库", "id": 5, "visible": 1, "hint": ""},
    {"name": "收款日期", "id": 6, "visible": 1, "hint": ""},
    {"name": "备注", "id": 7, "visible": 1, "hint": "(可为空)"},
    {"name": "整单折扣", "id": 8, "visible": 1, "hint": ""},
    {"name": "优惠", "id": 9, "visible": 1, "hint": ""},
    {"name": "添加费用", "id": 10, "visible": 1, "hint": ""},
    {"name": "添加收款", "id": 11, "visible": 1, "hint": ""},
    {"name": "收货地址", "id": 12, "visible": 1, "hint": ""},
    {"name": "物流公司", "id": 13, "visible": 1, "hint": ""},
    {"name": "物流单号", "id": 14, "visible": 1, "hint": ""},
    {"name": "代收", "id": 15, "visible": 1, "hint": ""},
    {"name": "附件信息", "id": 16, "visible": 1, "hint": ""},
  ];

  static List<BillHeaderItem> getHeaders({List<int> onContainId}) {
    List<BillHeaderItem> items = [];
    headers.forEach((v) {
      BillHeaderItem item = BillHeaderItem(v);
      if (onContainId == null || onContainId.contains(item.id))
        items.add(item);
    });
    return items;
  }

  static void save() {
    LocalStorage.save(_key, Utils.jsonListToString(headers));
  }

  static bool _loaded = false;

  static load() async {
    if (_loaded) return;
    _loaded = true;
    String data = await LocalStorage.get(_key);
    if (data != null && data.isNotEmpty) {
      headers = Utils.parseJsonAsList(data);
    }
  }
}

class BillHeaderItem extends DataBase {
  BillHeaderItem(Map data) : super(data);

  int get id => getItemInt("id");
  String get name => getItem("name");
  String get hint => getItem("hint");

  bool get visible => getItemInt("visible") == 1;
  set visible(value) => setItem("visible", value == true ? "1" : null);
}