import 'package:grasp_flutter_app/utils/model/data_base.dart';

/// 单据类型数据存取对象
class BillTypesDao {
  static List<Map> _data = [
    {
      "name": "进货",
      "items": [
        {"name": "进货申请单"},{"name": "进货入库单"},{"name": "进货退货单"},
      ]
    },
    {
      "name": "销售",
      "items": [
        {"name": "销售申请单"},{"name": "销售出库单"},{"name": "销售退货单"},
      ]
    },
    {
      "name": "库存",
      "items": [
        {"name": "调拨单"},{"name": "拆装单"},{"name": "其他入库单"},{"name": "其他出库单"},{"name": "库存调整单"},{"name": "成本调整单"},
      ]
    },
    {
      "name": "财务",
      "items": [
        {"name": "付款单"},{"name": "收款单"},{"name": "费用单"},{"name": "收入单"},{"name": "会计凭证"},
      ]
    },
  ];

  static List<Map> getData() {
    return _data;
  }

  static List<BillTypeList> getBillTypes() {
    List<BillTypeList> items = [];
    _data.forEach((v) {
      items.add(BillTypeList(v));
    });
    return items;
  }
}

class BillTypeList extends DataBase {
  BillTypeList(Map data) : super(data) {
    items = getList<BillTypeItem>("items", (v) {
      return BillTypeItem(v);
    });
  }
  String get name => getItem("name");
  List<BillTypeItem> items;
  int get selectCount => _getSelectCount();

  int _getSelectCount() {
    if (items == null) return 0;
    int count = 0;
    items.forEach((item) {
      if (item.checked) count++;
    });
    return count;
  }
}

class BillTypeItem extends DataBase {
  BillTypeItem(Map data) : super(data);
  String get name => getItem("name");
  bool get checked => getItemInt("checked") == 1;
    set checked(value) => setItem("checked", value == true ? "1" : null);
}